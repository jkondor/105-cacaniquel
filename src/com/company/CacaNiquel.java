package com.company;

import java.util.ArrayList;
import java.util.Random;

public class CacaNiquel {
    private int numeroSlots;
    private int recorde;

    public CacaNiquel(int numeroSlots) {
        this.numeroSlots = numeroSlots;
        this.recorde = 0;
    }

    public int getNumeroSlots() {
        return numeroSlots;
    }

    public int getRecorde() {
        return recorde;
    }

    public void setRecorde(int recorde) {
        this.recorde = recorde;
    }

    public void realizarSorteio(){
        String resultado = "";
        int valorTotal = 0;
        boolean querJogar;
        ArrayList<Slots> sorteio;

        querJogar =  EntradaSaida.iniciarAposta();

        while (querJogar){

            sorteio = sortear();

            valorTotal = calcularPontuacao(sorteio);

            for (Slots leSorteio: sorteio) {
                resultado = resultado + leSorteio + " / ";
            }

            EntradaSaida.mostrarSaida(resultado + valorTotal);


            atualizarRecorde(valorTotal);

            EntradaSaida.mostrarSaida("Recorde: " + getRecorde());

            querJogar =  EntradaSaida.iniciarAposta();

            //limpa variaveis para novo sorteio.
            resultado = "";
            sorteio.clear();

        }

        EntradaSaida.mostrarSaida("Obrigado por jogar em nosso Caça Níquel!");
    }

    public ArrayList<Slots> sortear(){
        ArrayList<Slots> sorteio = new ArrayList<>();

        for (int i = 0; i< (Slots.values().length - 1); i++) {
            Random random = new Random();
            sorteio.add(Slots.values()[random.nextInt(Slots.values().length)]);
        }

        return sorteio;
    }
    public int calcularPontuacao(ArrayList<Slots> sorteio){
        int somaSlots = 0;
        boolean tresIguais = true;

        for ( Slots valorSlot: sorteio) {
            somaSlots += valorSlot.getPontuacao();
        }


        for (int i = 0; i< (sorteio.size()); i++) {
            if (sorteio.get(0) != sorteio.get(i)){
                tresIguais = false;
            }
        }

        if (tresIguais) {
                somaSlots = somaSlots * 100;
        }
        return somaSlots;
    }


    public void atualizarRecorde(int valor){

        if (valor > getRecorde()){
            setRecorde(valor);
        }
    }
}
