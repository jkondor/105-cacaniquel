package com.company;

import org.omg.CORBA.PUBLIC_MEMBER;

import java.util.Scanner;

public class EntradaSaida {

    public static boolean iniciarAposta(){
        char querJogar;

        Scanner leitura = new Scanner(System.in);

        System.out.print("");
        System.out.print("Você deseja jogar (S/N):");
        querJogar = leitura.next().charAt(0);
        if (querJogar == 's' || querJogar == 'S') {
            return true;
        } else {
            return false;
        }
    }

    public static void mostrarSaida(String mensagem){
        System.out.println(mensagem);
    }
}